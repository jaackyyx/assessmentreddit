package android.hovsepianj.assessmentreddit;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

public class AssessmentActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<Todoitem> todoitems = new ArrayList<Todoitem>();


    @Override
    public void onPostSelected(Uri redditPostUri) {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);


    }

}
