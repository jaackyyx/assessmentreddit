package android.hovsepianj.assessmentreddit;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder> {

    public String[] todoPosts;

    public TodoAdapter(String[] todoPosts){
        this.todoPosts = todoPosts;

    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, final int i) {
        holder.titleText.setText(todoPosts[i]);

        }

    @Override
    public int getItemCount() {
        return todoPosts.length;
    }
}
