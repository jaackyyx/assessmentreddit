package android.hovsepianj.assessmentreddit;

public class Todoitem {
    public String title;
    public String dateAdd;
    public String dateDue;
    public String category;

    public Todoitem(String title, String dateAdd, String dateDue, String category) {
        this.title = title;
        this.dateAdd = dateAdd;
        this.dateDue = dateDue;
        this.category = category;
    }
}
