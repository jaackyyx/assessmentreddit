package android.hovsepianj.assessmentreddit;

import android.app.Activity;
import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

        import java.util.ArrayList;

public class ToDoFragment extends Fragment {

    private ActivityCallback activityCallback;
    private AssessmentActivity activity;
    private RecyclerView recyclerView;

    String[] todoPosts = new String[]{"Hello", "Goodbye", "It's me", "How are you?"};

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activityCallback = (ActivityCallback)activity;
        this.activity = (AssessmentActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

             Todoitem i1 = new Todoitem("Hey", "1/02/15", "1/03/15", "Nice");
             Todoitem i2 = new Todoitem("Hello", "1/04/15", "1/05/15", "Formal");
             Todoitem i3 = new Todoitem("Hi", "1/06/15", "1/07/15", "Greetings");
             Todoitem i4 = new Todoitem("Howdy", "1/08/15", "1/09/15", "Salutations");

             activity.todoitems.add(i1);
             activity.todoitems.add(i2);
             activity.todoitems.add(i3);
             activity.todoitems.add(i4);

             updateUserInterface();

               return view;
            }
    public void updateUserInterface() {
        TodoAdapter adapter = new TodoAdapter(todoPosts);
        recyclerView.setAdapter(adapter);
    }
}
